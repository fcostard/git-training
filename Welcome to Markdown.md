# Welcome to Markdown

[this_document]: https://christophelarsonneur.gitlab.io/markdown-demo/LAB-4/ "gitlab rendered version (with TOC)"
[gitlab]: https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md "here"
[gitlab_rend]: https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md "gitlab rendered version (with TOC)"
[here]: https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md?plain=1 "here"
[md2pdf]: https://md2pdf.netlify.app/ "md2pdf"
[pandoc]: https://pandoc.org/demos.html#examples "pandoc"
[html_converter]: https://markdowntohtml.com/ "html_converter"
[markdown_linux]: https://linuxhint.com/convert-markdown-files-linux/ "markdown_linux"
[msword_converter]: https://cloudconvert.com/md-to-docx "msword_converter"

*Lab-4 exercice:*

[This document][this_document] is fully readable from [gitlab][gitlab].

The goal is to reproduce the [gitlab rendered version (with TOC)][gitlab_rend] as is (with this blockquote) but on VSC with the preview.

You can copy/paste this page as is to create your version. Then you will need to add markdown markup to proper format the output on the preview.

The source code is [here][here]. Do open it only if you want to compare your version and your version.

Remember to finally sign your document with the inline html code.

[[TOC]]

![markdown](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Octicons-markdown.svg/240px-Octicons-markdown.svg.png)

Markdown is a great way to write technical document **readable in text mode**. It is easier and faster than writing a word processing application (like microsoft word)

We can generate it in different format.

Few examples:

- PDF : [Online md2pdf][md2pdf], [pandoc][pandoc]
- HTML : [Online converter in HTML][html_converter], [Linux markdown][markdown_linux], [pandoc][pandoc]
- MSWord : [Online converter to MSWord][msword_converter], [pandoc][pandoc]

## One detailled option - Markdown on Linux with Pandoc

On Linux, you can do any kind of conversion from markdown format to HTML, PDF, etc, thanks to pandoc.

## Install on Fedora

Installation:

<details>
<summary>my summary</summary>
```bash
$ sudo dnf -y install pandoc
$
```
</details>

## Converting

Running The converter:

```shell
$ pandoc README.md --pdf-engine=xelatex -o example13.pdf
$
```

Thank you
<div style="text-align:right">F Costard</div>
